#include "utils.h"
#include "DefaultGA.h"
#include "BotLeftHeuristic.h"
#include "InputHandler.h"
#include "main.h"

class GAQueue
{
    private:
        std::queue<int*> instanceInfo;

    public:
        void runNext() {
            int *instance_meta_info = instanceInfo.front();

            //get input instance
            int  instanceNumber       = instance_meta_info[PROB_INSTANCE];
            char instanceFile[PATH_MAX_SIZE];
            sprintf(instanceFile, "input/Class_%02d.2bp", instance_meta_info[PROB_CLASS]);
            InputHandler cp1(instanceFile, INSTANCES_PER_FILE);

            //open files for writing
            char output_path[PATH_MAX_SIZE];
            sprintf(output_path, PACKING_DATA_OUTPUT_PATH);
            strcat(output_path, "/");
            char problemInstanceName[PATH_MAX_SIZE];
            sprintf(problemInstanceName, "C%02d_i%d_%dpopsize_%dgen_%dmr_%ds", instance_meta_info[PROB_CLASS],
                    instance_meta_info[PROB_INSTANCE], instance_meta_info[PROB_POP_SIZE], instance_meta_info[PROB_N_GEN],
                    instance_meta_info[PROB_MUT_RATE], instance_meta_info[PROB_TIME_LIM]);
            strcat(output_path, problemInstanceName);
            strcat(output_path, ".txt");

            // no need to close the file
            std::ofstream packing_info_file(output_path);

            if(!packing_info_file) {
                printf("Error prepraing file!\n");
                exit(1);
            }

            time_t start_time,end_time;
            time (&start_time);
            BotLeftHeuristic decodeInstance(cp1.getSizesArray(instanceNumber), cp1.getSize(instanceNumber));
            int *bestChromossome;
            
            /* 
             * This run the Default solver and prints the results!
             */
            DefaultGA GA(instance_meta_info[PROB_POP_SIZE], instance_meta_info[PROB_N_GEN],
                    instance_meta_info[PROB_MUT_RATE], decodeInstance.getNumOfItens(), decodeInstance);
            printf("=====================\n");
            printf("Starting SERIAL solver for %s\n", problemInstanceName);
            GA.solve(instance_meta_info[PROB_TIME_LIM]);
            bestChromossome = GA.getFinalBestChromossome();
            time (&end_time);
            int buffer_stimated_size = (40 * decodeInstance.getNumOfItens() * 2);
            char buffer[buffer_stimated_size];
            decodeInstance.printPoistionOfItems(bestChromossome, buffer);
            //do not print this run
            //packing_info_file << buffer;

            double dif = difftime (end_time,start_time);
            printf ("Number of bins used: %d\n", decodeInstance.numOfBins(bestChromossome));
            printf ("Elasped time is %.1lf seconds.\n", dif );
            printf ("Number of generations: %d\n", GA.getNumOfPassedGens());
            double bufferUsed = 100*((double)strlen(buffer)) / ((double)buffer_stimated_size);
            printf ("Buffer used: [%d]/[%d] (%.2f%%)\n", (int)strlen(buffer), buffer_stimated_size, bufferUsed);
            printf ("=================\n");

            /* 
             * This run the island soulver and prints the results!
             */
            time (&start_time);
            DefaultGA islandGA(instance_meta_info[PROB_POP_SIZE], instance_meta_info[PROB_N_GEN],
                    instance_meta_info[PROB_MUT_RATE], decodeInstance.getNumOfItens(), decodeInstance);
            printf("=====================\n");
            printf("Starting PARALLEL solver for %s\n", problemInstanceName);
            islandGA.islandSolve(instance_meta_info[PROB_TIME_LIM]);
            bestChromossome = islandGA.getFinalBestChromossome();
            time (&end_time);
            decodeInstance.printPoistionOfItems(bestChromossome, buffer);
            packing_info_file << buffer;

            dif = difftime (end_time,start_time);
            printf ("Number of bins used: %d\n", decodeInstance.numOfBins(bestChromossome));
            printf ("Elasped time is %.1lf seconds.\n", dif );
            printf ("Number of generations: %d\n", islandGA.getNumOfPassedGens());
            bufferUsed = 100*((double)strlen(buffer)) / ((double)buffer_stimated_size);
            printf ("Buffer used: [%d]/[%d] (%.2f%%)\n", (int)strlen(buffer), buffer_stimated_size, bufferUsed);
            printf ("=================\n");

            free(instanceInfo.front());
            instanceInfo.pop();
        }

        int isEmpty(){
            return instanceInfo.empty();
        }

        void push(int problemClass, int problemInstance, int populationSize,
                int numOfGenerations, int mutationRate, int timeLimitInSeconds){
            int *meta_info           = (int*)malloc(N_INSTANCE_META_INFO * sizeof(int));
            meta_info[PROB_CLASS]    = problemClass;
            meta_info[PROB_INSTANCE] = problemInstance;
            meta_info[PROB_POP_SIZE] = populationSize;
            meta_info[PROB_N_GEN]    = numOfGenerations;
            meta_info[PROB_MUT_RATE] = mutationRate;
            meta_info[PROB_TIME_LIM] = timeLimitInSeconds;

            instanceInfo.push(meta_info);
        }
};

int main()
{
    /* seeds the pseudo-random number generator */
    set_seed(time(NULL));

    /* Queue everything we wanna run */
    GAQueue instQueue;

    /*
     * Params:
     * Problem class
     * Problem instance
     * Population size
     * Number of generations
     * Mutation Rate
     * Time limit in seconds
     */
    instQueue.push(1, 5, 1500,5000,5,180);
    instQueue.push(1, 10, 1500,5000,5,180);
    instQueue.push(1, 15, 1500,5000,5,180);
    instQueue.push(1, 20, 1500,5000,5,180);
    instQueue.push(1, 25, 1500,5000,5,180);

    /* run everything */
    while(!instQueue.isEmpty()){
        instQueue.runNext();
    }

    return 0;
}

