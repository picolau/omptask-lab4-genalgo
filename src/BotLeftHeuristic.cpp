#include "BotLeftHeuristic.h"

BotLeftHeuristic::BotLeftHeuristic(int ** itemPack, int numOfItems){
    BotLeftHeuristic::itemPack = itemPack + 1; // does not include BIN
    // so we take from position
    // 1 and so forth
    BotLeftHeuristic::numOfItems = numOfItems - 1; // since we skiped the bin

    /* saves bin info */
    BotLeftHeuristic::bin_w = itemPack[0][W];
    BotLeftHeuristic::bin_h = itemPack[0][H];
}

BotLeftHeuristic::~BotLeftHeuristic(){

}

int BotLeftHeuristic::tryToFit(int  *permutation, int ** itemPack, int fitSoFar, int numOfItems, int  bin_w, int  bin_h, int verbose_pos){
    char* buffer = NULL;
    if(verbose_pos){
        printf("ERROR: can not verbose item pos withiout especified buffer!\n");
        exit(1);
    }

    return BotLeftHeuristic::tryToFit(permutation, itemPack, fitSoFar, numOfItems, bin_w, bin_h, verbose_pos, buffer);
}

int BotLeftHeuristic::tryToFit(int  *permutation, int ** itemPack, int fitSoFar, int numOfItems, int  bin_w, int  bin_h, int verbose_pos, char* buffer){
    /* try to pack level by level */
    int freeWidth = bin_w;
    int highestItemInLevel = 0;
    int i = fitSoFar;
    int *currentItem;
    while(i < numOfItems){
        currentItem = itemPack[(int)permutation[i]];
        /*  the item does not fit this level */
        if(currentItem[H] > bin_h){
            break;
        }

        /* check if item fits the width */
        if(currentItem[W] <= freeWidth) {
            /* update level hight */
            if(currentItem[H] > highestItemInLevel)
                highestItemInLevel = currentItem[H];
            if(verbose_pos)
                sprintf(buffer+strlen(buffer), "[[Item %d [h=%d][w=%d]FIT @ w=%d | h=%d ]]\n", i, currentItem[H], currentItem[W],
                                                                       BotLeftHeuristic::bin_w-freeWidth,
                                                                       BotLeftHeuristic::bin_h-bin_h);
            freeWidth -= currentItem[W];
            i++;

            /* if the item does not fit the width,
             * try to put it on the level 'above' */
        } else {
            i = tryToFit(permutation, itemPack,  i, numOfItems, bin_w, (bin_h-highestItemInLevel), verbose_pos, buffer);
            break;
        }
    }

    return i;
}

int BotLeftHeuristic::numOfBins(int* permutation) {
    int fitSoFar = 0;
    int i = 0;
    while(fitSoFar < numOfItems) {
        fitSoFar = tryToFit(permutation, itemPack, fitSoFar, numOfItems, bin_w, bin_h, FALSE);
        i = i + 1;
    }

    return i;
}


void BotLeftHeuristic::printPoistionOfItems(int* permutation, char* buffer) {
    int fitSoFar = 0;
    int i = 0;
    sprintf(buffer, "Starting FIT to BIN of H=%d, W%d\n", bin_h, bin_w);

    while(fitSoFar < numOfItems) {
        sprintf(buffer+strlen(buffer), "FIT: Packing bin: %d\n", i);
        fitSoFar = tryToFit(permutation, itemPack, fitSoFar, numOfItems, bin_w, bin_h, TRUE, buffer);
        i = i + 1;
        sprintf(buffer+strlen(buffer), "FIT: =================\n");
    }

    sprintf(buffer+strlen(buffer), "Total used bins: %d\n", i);
}

int BotLeftHeuristic::getNumOfItens() {
    return BotLeftHeuristic::numOfItems;
}

