#ifndef DEFAULT_GA_H
#define DEFAULT_GA_H
#include "utils.h"
#include "BotLeftHeuristic.h"
#include <iostream>
#include <cstring>
#include <algorithm>
#include <vector>
//#define VERBOSE

class DefaultGA
{
    private:

        long int bestValue; // keeps best balue so far
        int generations; // number of generations before stopping
        int popSize; // number of individuals
        int chromosomeSize; // size of a chromosome
        int mutationRate; // change of a mutation ocurring, per locus
        int *bestChromossome;
        int passedGens;
        BotLeftHeuristic PlaceHeuristic;

    public:
        DefaultGA(int popSize, int generations, int mutationRate, int chromosomeSize, BotLeftHeuristic instance);
        ~DefaultGA();
        void crossoverOrdered (int *parentA, int *parentB, int *childA, int *childB);
        void sequenceMutate(int *chromosome, int rate);
        void generatePermutation(int *chromosome);
        void solve(int max_time_seconds);
        void islandSolve(int max_time_seconds);

        void showPopulation(int **population, int popSize);
        int getNumOfPassedGens();
        void elitistGeneration(int **population, int **offspring, int popSize);
        void tournamentSelection(int **population, int *parentA, int *parentB, int tournament_dimension, int size);
        static bool mysort (int *chromosome1, int *chromosome2);
        void showChromosome(int *chromosome);
        int fitness (int *chromosome);
        int *getBestChromossome(int **population, int size);
        int *getBestChromossomeOf4islands(int **island1, int **island2, int **island3, int **island4, int popSize);
        int *getFinalBestChromossome();
        int getBestChromossomePos(int **population, int size);
        void generateInitialPopulation(int **population, int popSize);

        /*Darwin Island Methods*/
        void fourIslandsMigration(int **island1, int **insland2, int **island3, int **insland4, int islandSize);
        void runIslandGeneration(int **islandPop, int **islandOffspring, int popSize, int *bestIndividual);
};

#endif

