#ifndef BOT_LEFT_FILL_H
#define BOT_LEFT_FILL_H
#include "utils.h"
#include <iostream>
#include <cstring>
#include <algorithm>
#include <vector>

//#define VERBOSE

#define H 0
#define W 1

class BotLeftHeuristic
{
    private:
        int tryToFit(int *permutation, int** itemPack, int fitSoFar, int numOfItems, int bin_w, int bin_h, int verbose_pos);
        int tryToFit(int *permutation, int** itemPack, int fitSoFar, int numOfItems, int bin_w, int bin_h, int verbose_pos, char* buffer);
        int numOfItems;
        int bin_w;
        int bin_h;
        int **itemPack;

    public:
        //assumes that the position ItemPack[0] has info about the bin
        //ItemPack[i] has form: [Hight of i, Width of i]
        //with const definition, we can use: itemPack[i][W]
        BotLeftHeuristic(int** ItemPack, int numOfItems);
        ~BotLeftHeuristic();


        int numOfBins(int* permutation);
        void printPoistionOfItems(int* permutation, char* buffer);
        int getNumOfItens();
};

#endif

