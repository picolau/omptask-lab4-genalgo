#ifndef INPUT_HANDLER_H
#define INPUT_HANDLER_H
#include "utils.h"
#include <iostream>
#include <fstream>
#include <cstring>
#include <algorithm>
#include <vector>

//#define VERBOSE

#define H 0
#define W 1

struct inputInstance {
    int relativeNumber;
    int absoluteNumber;
    int problemClass;
    int sizesArray_size;
    int ** sizesArray;
};

class InputHandler
{
    private:
        int numOfInstances;
        inputInstance *inputInstances;

    public:
        InputHandler(char input_file_path[], int numOfInstances);
        ~InputHandler();
        int getInstanceClass(int i);
        int  **getSizesArray(int i);
        int getSize(int i);
        int getNumOfInstances();
};

#endif

