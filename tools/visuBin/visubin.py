import numpy as np
import matplotlib.pyplot as plt
import pprint
import re
import os
import glob
import math
import sys

def main():
    #generate a image for each file in the input folder
    in_path = 'input/'
    for infile in glob.glob(os.path.join(in_path, '*')):
        print(infile)
        out_path = infile.replace('input', '', 1).replace('.txt', '', 1)
        generate_output(infile, out_path)

def generate_output(input_name, output_name):
    pp = pprint.PrettyPrinter()

    #assumes order:
    #bin number
    #item_id item_h item_w item_w_pos_in_bin item_h_pos_in_bin
    with open(input_name, 'r') as input_file:
        text_input = input_file.read()

    packingBinInfo = []

    splitByBin = text_input.split('bin')
    binSizeinfo = {}
    input_first_line = re.findall('\d+', splitByBin.pop(0))
    #assumes sizeH comes first, and then sizeW
    binSizeinfo['sizeH'] = input_first_line[0]
    binSizeinfo['sizeW'] = input_first_line[1]
    #print("bin info = ", binSizeinfo)

    for packingBin in splitByBin:
        packNumber = (re.findall('\d+', packingBin))
        if packNumber:
            #get bin number
            binObject = {}
            binObject['binNum'] = packNumber.pop(0)

            #get each item in bin
            binObject['items'] = []

            while packNumber:
                itemObject = {}
                itemObject['id'] = packNumber.pop(0)
                itemObject['sizeH'] = packNumber.pop(0)
                itemObject['sizeW'] = packNumber.pop(0)
                itemObject['posW'] = packNumber.pop(0)
                itemObject['posH'] = packNumber.pop(0)
                binObject['items'].append(itemObject)

            packingBinInfo.append(binObject)

    #pprint.pprint(packingBinInfo)

    # settings
    numOfBins = len(packingBinInfo)
    #array of subplots
    nrows = math.ceil(math.sqrt(numOfBins))
    ncols = math.ceil(numOfBins/nrows)
    w=int(binSizeinfo['sizeW'])
    h=int(binSizeinfo['sizeH'])
    #TODO: decide if this size should be dynamic
    figsize = [20, 20]     # figure size, inches

    # prep (x,y) for extra plotting on selected sub-plots
    #xs = np.linspace(0, 2*np.pi, 60)  # from 0 to 2pi
    #ys = np.abs(np.sin(xs))           # absolute of sine

    # create figure (fig), and array of axes (ax)
    fig, ax = plt.subplots(nrows=nrows, ncols=ncols, figsize=figsize)

    #build raster representation of bins
    binRasterList = []
    for someBin in packingBinInfo:
        binNum    = someBin['binNum']
        binItems  = someBin['items']
        binRaster = np.zeros((h,w), dtype=int)
        #paint the items in box
        for item in binItems:
            h_i = int(item['posH'])
            w_i = int(item['posW'])
            h_max = int(h_i) + int(item['sizeH'])
            w_max = int(w_i) + int(item['sizeW'])
            range_h = h_max-h_i
            range_w = w_max-w_i
            colorCode = int(item['id']) + 3
            binRaster[h_i:h_max,w_i] = colorCode
            binRaster[h_i:h_max,w_max-1] = colorCode
            binRaster[h_i,w_i:w_max] = colorCode
            binRaster[h_max-1,w_i:w_max] = colorCode

        binRasterList.append(binRaster)

    #print("len: ", len(binRasterList))

    # plot simple raster image on each sub-plot
    for i, axi in enumerate(ax.flat):
        # i runs from 0 to (nrows*ncols-1)
        # axi is equivalent with ax[rowid][colid]
        img = np.zeros((h,w), dtype=int)
        if i < len(binRasterList):
            img=binRasterList[i]

        #heatmap = axi.pcolor(img)
        vmax = (len(binRasterList)+1)
        axi.imshow(img, vmin=0, vmax=vmax, cmap="tab20c")
        axi.invert_yaxis()
        axi.set_title("Bin: "+str(i))

    plt.tight_layout(True)
    #plt.show()
    plt.savefig("output/" + output_name + ".pdf", bbox_inches='tight')

if __name__== "__main__":
    main()

